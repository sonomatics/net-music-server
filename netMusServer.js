
var net = require('net')

var PORT = 8124

var users = {} // { username:socket }

// array of keywords which are ineligible for use as usernames
var reservedKeywords = [ "login", "logout", "server", "all",
"start", "stop", "mirror" ]

// external piece/extension module
var curPiece = 0

// extensions registered to individual usernames (not including "all")
// plus reserved keywords "login", "logout", "mirror", and "all"
// reserved keywords "start" and "stop" are not supported by this mechanism
// instead, use start() / stop() functions (exported by examplePiece.js template)
var extensionHandlers = {}

// extensions which are registered to keywords sent to "server" address
// e.g. register "mode" to capture "server mode"
var serverExtensionHandlers = {}

// optional password for "mirror" functionality is passed at startup
// preceded by -m flag
// mirroring copies *all* network traffic to the registered socket(s)
// in the form "<sender> <receiver> <messageContents>;"
// in the absence of a password, "mirror" requests are declined
// while we are checking for startup args we  look for an optional port number
// preceded by a -p flag; otherwise stay with the default 8124
var mirrorPassword = false
for(i = 2; i < process.argv.length; i++)
{
    if (process.argv[i] == "-m" && process.argv[i + 1])
    {
        mirrorPassword = process.argv[i + 1]
    }
    else if (process.argv[i] == "-p" && process.argv[i + 1])
    {
        {
            PORT = process.argv[i + 1]
        }
    }
}
var mirrors = {} // { unique socket ID:socket }

// shadows - can send messages addressed to a specific user to additional sockets
// first, the *actual* user logs in, and specifies a password
// then, additional sockets log in with the same username and password
// the shadowing sockets may not send messages
// shadowing sockets receive all messages addressed to the username they shadow
// (including messages sent to "all")
// if the initial login does not specify a password, shadowing is disabled for that username
var shadows = {} // { unique socket ID:username }
var shadowSockets = {} // { unique socket ID:socket}
var shadowPasswords = {} // { username:password }

// a server instance for each connected socket
net.createServer(function(sock) {

    // disable Nagle's Algorithm
    sock.setNoDelay(true)

    // synthesize a unique id out of this socket's address
    var sockId = String(sock.remoteAddress +':'+ sock.remotePort)
    console.log('CONNECTED: ' + sockId)

    var thisUser = String("")

    // shadowUsers may not send messages or change login
    var shadowUser = false

    // only the user who sends a "start" command may send the matching "stop"
    var startUser = false

    var destination = String("")
    var curMsg = String("")

    // add a 'data' event handler to this instance of socket
    sock.on('data', function(data) {

        // parse the 'FUDI' input
        var inStr = String(data)
        var destinationComplete = false
        for (var i = 0; i < inStr.length; i++)
        {
            var c = inStr[i]

            // make sure all "whitespace" is an actual whitespace
            if (c == '\t' || c == '\n')
            c = ' '

            // if nothing in current message yet, we are in middle of parsing the destination
            if (!destinationComplete)
            {
                // destination ends on whitespace or semicolon
                if (c == ' ' || c == ';')
                {
                    // username is complete
                    if (destination.length)
                    {
                        destinationComplete = true
                    }
                    // but if destination is empty, then we just skip over initial white space...
                }
                else
                {
                    destination += c
                }
            }
            else if (c != ';' && (curMsg.length || c != ' ')) // skip over initial whitespace
            {
                curMsg += c
            }

            // end of message
            if (c == ';')
            {
                // trim off any extra whitespace on ends
                curMsg = curMsg.trim()

                // first manage keywords: "login", "mirror", "all", etc.
                if (reservedKeywords.includes(destination))
                {
                    if (destination == "mirror")
                    {
                        // "mirror <password>"
                        if (!mirrorPassword)
                        {
                            sendError("mirroring not permitted by this instance")
                        }
                        else if (thisUser)
                        {
                            sendError("logged-in users may not invoke mirroring")
                        }
                        else if (!curMsg)
                        {
                            sendError("mirror requests must include password")
                        }
                        else if (curMsg != mirrorPassword)
                        {
                            sendError("mirroring password incorrect")
                        }
                        else if (mirrors[sockId])
                        {
                            sendError("socket is already mirroring")
                        }
                        else
                        {
                            mirrors[sockId] = sock
                            sendToAll("server", "socket: \'" + sockId + "\' now mirroring all network traffic")
                        }
                    }
                    else if (destination == "login")
                    {
                        // "login <username> <optional mirroring password>"
                        var loginComponents = curMsg.split(" ")

                        var newUser = loginComponents[0]
                        var inPassword = loginComponents[1]

                        if (mirrors[sockId])
                        {
                            sendError("login not permitted for mirroring sockets")
                        }
                        else if (0 == newUser.length)
                        {
                            sendError("no username to login")
                        }
                        else if (newUser == thisUser)
                        {
                            sendError("\'" + newUser + "\' is already using this socket")
                        }
                        // test proposed username against keywords reserved by server
                        else if (reservedKeywords.includes(newUser))
                        {
                            sendError("keyword \'" + newUser + "\' is reserved by the server")
                        }
                        // if username is already in use, this is a request to shadow
                        else if (users[newUser])
                        {
                            if (!shadowPasswords[newUser])
                            {
                                sendError("shadowing not permitted by this user")
                            }
                            else if (shadowPasswords[newUser] != inPassword)
                            {
                                sendError("shadowing password incorrect")
                            }
                            else
                            {
                                shadows[sockId] = newUser
                                shadowSockets[sockId] = sock
                                thisUser = newUser
                                shadowUser = true
                                sendToAll("server", "socket " + sockId + " now shadowing user: \'" + thisUser + "\'")
                            }
                        }
                        else
                        {
                            // changing username
                            if (thisUser.length > 0)
                            {
                                // first logout from old user identity
                                logout()
                            }

                            // and then login to new identity
                            thisUser = newUser
                            users[thisUser] = sock
                            if (inPassword)
                            {
                                shadowPasswords[thisUser] = inPassword
                            }

                            // let everyone know, including ourselves
                            sendToAll("server", "user: \'" +  newUser + "\' joined")
                        }
                    }
                    else if (destination == "logout")
                    {
                        // optional "logout" keyword (for users, shadows, mirrors)
                        logout() // handles extension, interface with mirrors / shadows
                    }
                    else if (thisUser.length == 0)
                    {
                        // check that username is registered before allowing other messages
                        // (only "login", "mirror", and "logout" are permitted prior to this check)
                        if (mirrors[sockId])
                        {
                            sendError("mirroring sockets may not send messages")
                        }
                        else
                        {
                            sendError("you must login before sending any other message")
                        }
                    }
                    else if (shadowUser)
                    {
                        // shadow users are not permitted to send messages except login/logout
                        sendError("shadow users are not permitted to send messages")
                    }
                    else if (destination == "all")
                    {
                        if (0 == curMsg.length)
                        {
                            sendError("message is empty")
                        }
                        else
                        {
                            if (destination == "all")
                            {
                                // send to all except ourselves
                                sendToAll(thisUser, curMsg)
                            }
                        }
                    }

                    // prepare to check for extensions and call as needed
                    let messageContents = curMsg.split(" ")

                    // "server", "stop", "start" need special extension handling
                    if (destination == "server")
                    {
                        // rather than registering "server" for extensions
                        // we register the keyword *after* "server"...
                        // "server" is the address, and the next parameter is the keyword
                        if (serverExtensionHandlers[messageContents[0]])
                        {
                            let keyword = messageContents[0]
                            messageContents.shift()
                            console.log("sending to handler: " + messageContents)
                            serverExtensionHandlers[keyword](thisUser, messageContents)
                        }
                        else
                        {
                            sendError("server keyword not recognized")
                        }
                    }
                    else if (destination == "start")
                    {
                        // no explicit handlers for "start"
                        // instead, call the piece's start() method
                        // and set up new keyword and user handlers there
                        if (0 == curMsg.length)
                        {
                            sendError("no piece name given to start")
                        }
                        else if (curPiece == 0)
                        {
                            // start given piece -- this assumes it is located in a "pieces" subdirectory
                            try {
                                curPiece = require("./pieces/" + curMsg)
                                curPiece.start()
                                startUser = true
                            } catch (exception) {
                                sendError("unable to load piece: file may not be present in ./pieces")
                            }
                        }
                        else
                        {
                            sendError("must stop currently running piece before starting another")
                        }
                    }
                    else if (destination == "stop")
                    {
                        // similarly, no explicit handlers for "stop"
                        // instead, call the piece's stop() method
                        // and remove piece-specific extensions here
                        if (curPiece)
                        {
                            if (startUser)
                            {
                                curPiece.stop()
                                curPiece = 0

                                // remove all handlers associated with the piece we are stopping
                                extensionHandlers = {}
                                serverExtensionHandlers = {}
                                startUser = false
                            }
                            else
                            {
                                sendError("only the initiating user may stop a piece")
                            }
                        }
                        else
                        {
                            sendError("no piece currently running to stop")
                        }
                    }
                    else
                    {
                        // extension handling for "login", "logout", "mirror", "all"
                        if (extensionHandlers[destination])
                        {
                            extensionHandlers[destination](thisUser, messageContents)
                        }
                    }

                    // send keyword-addressed messages to mirrors
                    // excepting messages addressed to "all", since sendToAll() mirrors
                    // mirror copies rewritten as <sender> <receiver> <messageContents>;
                    if (destination != "all")
                    {
                        for (var mirrorId in mirrors)
                        {
                            mirrors[mirrorId].write(thisUser + " " + destination + " " + curMsg + ";") // includes sender and "all"
                        }
                    }
                }
                else
                {
                    // not a reservedKeywords, so handle "regular" messages to users: forward to destination
                    if (shadowUser || mirrors[sockId])
                    {
                        sendError("shadow/mirror users are not permitted to send messages")
                    }
                    else if (users[destination])
                    {
                        if (0 == curMsg.length)
                        {
                            sendError("message is empty")
                        }
                        else
                        {
                            // forward message to addressee, and mirror as needed
                            sendToUser(thisUser, destination, curMsg, function(err) {
                                sendError(err)
                            })

                            // check if username is registered with extension handlers
                            if (extensionHandlers[destination])
                            {
                                // convert message into an array, leaving out ending semicolon
                                let messageContents = curMsg.split(" ")

                                // then forward the array contents to extension handlers
                                extensionHandlers[destination](thisUser, messageContents)
                            }
                        }
                    }
                    else
                    {
                        sendError("no such user")
                    }
                }

                // debugging
                console.log(destination + " " + curMsg)

                destination = String("")
                curMsg = String("")
                destinationComplete = false
            }
        }
    })

    // socket closing automatically initiates "logout"
    // (explicit "logout" keyword is available, but not required)
    sock.on('close', function(data) {
        logout()
        console.log('CLOSED: ' + sockId)
    })

    // handle an error ('close' event will be called immediately after)
    sock.on('error', function(err) {
        console.log('ERROR: ' + sockId + ": " + err)
    })

    // handle logout (triggered by "logout" keyword or by socket close)
    function logout()
    {
        // user "logout"
        if (thisUser.length > 0 && !shadowUser)
        {
            // check for sockets shadowing user
            let shadowCount = 0
            {
                for (let shadowID in shadows)
                {
                    if (shadows[shadowID] == thisUser)
                    {
                        shadowSockets[shadowID].write("server warning: shadowed user \'" + thisUser + "\' left. login with new username or disconnect;")
                        delete shadows[shadowID]
                        delete shadowSockets[shadowID]
                        shadowCount++
                    }
                }
            }

            // remove entry in shadow passwords dict (present for any logged-in user)
            delete shadowPasswords[thisUser]

            // then remove user
            delete users[thisUser]

            // notify remaining users
            sendToAll("server", "user \'" + thisUser + "\' left")
            if (shadowCount > 0)
            {
                sendToAll("server", "shadows of \'" + thisUser + "\' terminated")
            }
            console.log(thisUser + ' left')

            // call logout handler
            if (extensionHandlers["logout"])
            {
                extensionHandlers["logout"](thisUser, [])
            }
            thisUser = ""
        }

        // shadow "logout"
        else if (shadowUser)
        {
            delete shadows[sockId]
            delete shadowSockets[sockId]
            sendToAll("server", "socket " + sockId + " shadowing \'" + thisUser + "\' terminated")
            thisUser = ""
            shadowUser = false;
        }

        // mirror "logout"
        else if (mirrors[sockId])
        {
            delete mirrors[sockId]
            sendToAll("server", "socket " + sockId + " ended mirroring")
        }
    }

    // send error message to the user that caused the error
    function sendError(msg)
    {
        sock.write("server error: " + msg + ";")
    }

}).listen(PORT)

console.log('Server listening on port ' + PORT)


// EXPORTED FUNCTIONS TO BE USED BY CUSTOM PIECES / EXTENSIONS

// function which handles broadcast of messages addressed to "all"
function sendToAll(sender, messageContents)
{
    // send to all users/shadows, including the originating user ("sender") + its shadows
    for (var user in users)
    {
        users[user].write(sender + " " + messageContents + ";")
        for (let shadowId in shadows)
        {
            shadowSockets[shadowId].write(sender + " " + messageContents + ";")
        }
    }

    // mirrors receive all network traffic
    // rewritten as <sender> <receiver> <messageContents>;
    for (var mirrorId in mirrors)
    {
        mirrors[mirrorId].write(sender + " all " + messageContents + ";") // includes sender and "all"
    }
}

// function which handles delivery of messages addressed to a single user
function sendToUser(sender, receiver, messageContents, onError)
{
    // send to addressee
    var destSocket = users[receiver]
    if (destSocket)
    {
        // send to addressee + any shadows, rewriting addressee to sender
        destSocket.write(sender + " " + messageContents + ";")
        for (let shadowId in shadows)
        {
            if (shadows[shadowId] == receiver)
            {
                shadowSockets[shadowId].write(sender + " " + messageContents + ";")
            }
        }

        // mirrors receive all network traffic (excepting invalid addresses)
        // rewritten as <sender> <receiver> <messageContents>;
        for (var mirrorId in mirrors)
        {
            mirrors[mirrorId].write(sender + " " + receiver + " " + messageContents + ";") // includes sender and receiver
        }
    }
    else
    {
        onError("there is no user \'" + receiver + "\'")
    }
}

// registers a message handler
exports.on = function (key, handler)
{
    extensionHandlers[key] = handler
}

exports.onServer = function (key, handler)
{
    serverExtensionHandlers[key] = handler
}

exports.getUsers = function ()
{
    return Object.keys(users)
}

exports.sendToAll = sendToAll
exports.sendToUser = sendToUser
