# net-music-server

A simple node.js server for delivering messages to multiple participants. We intend this server for use in networked music ensemble performances, where the exchange of networked data serves as a complement to, or enrichment of, the communication-through-sound of collective musicmaking. In addition to its standard message-passing capabilities, the server can optionally load modules which supply data structures, logic, etc. specific to a particular composition or musical approach.

Clients connect with a TCP socket (using port 8124 by default) via their
native networking (for example, the [netsend/netreceive](http://write.flossmanuals.net/pure-data/send-and-receive/) objects in [Pure Data](http://msp.ucsd.edu/software.html), or the [Network library](https://processing.org/reference/libraries/net/index.html) supplied by [Processing](https://processing.org)).

Specific features of this project include:
+ private delivery of messages addressed to specific clients
+ FUDI protocol: lightweight, simple to implement, and easily adapted to a variety of musical scenarios
+ easy extension of server behavior for specific implementations, compositions, etc.
+ does not require installation of software on clients
+ uses TCP

# Running

After [installing node](https://nodejs.org/en/download/current/), simply run the command:
```
node netMusServer.js
```

There are two optional arguments you may pass to the server using flags. The `-m` flag is used to specify a password. If a password is supplied, password-protected "mirroring" of all network traffic through the server is enabled.

```
node netMusServer.js -m globalMirroringPassword
```

See below for additional details about mirroring.

The `-p` flag is used to specify a port number for the server.

```
node netMusServer.js -p 4444
```

In the absence of a user-specified port, the server defaults to port 8124.

# Messaging

Messaging is in Pure Data's [FUDI format](https://en.wikipedia.org/wiki/FUDI).

```
user p0 p1 … pN;
```
where
``user`` is the client the server will deliver the message to.
upon receiving a message, ``user`` will be the name of the sender.

Parameters (``p0 p1 ...``) are strings (representing text or numerical data).

``p0`` is generally a message name, followed by other parameters.
However, the server is agnostic about message formats, and participants can agree on other
usages.

Sending a message to user ``all`` will deliver the message to everyone, including yourself.

It is also possible to address and send a message to yourself.

Note that the ``;`` is automatically handled when using Pure Data's
[netsend/netreceive](http://write.flossmanuals.net/pure-data/send-and-receive/) objects.

# Login / Logout

Before sending/receiving a message, you must first send a message to ``login``:

```
login matt;
```

You cannot login with a username already taken (or with a reserved keyword  - 'all', 'server', 'mirror', 'login', 'logout', 'start', or 'stop').

Optionally, you may supply a password at login; if provided, other clients can login with the same username and password and "shadow," or receive copies of all the messages sent to this username (including messages sent to "all"). Shadowing clients cannot send messages. For more about shadowing, please see below.

```
login matt mattsShadowingPassword;
```

The ``logout`` message is optional. Logged-in users can switch usernames by sending a new ``login`` message with a different username, and closing the socket at the client end will automatically trigger logout behavior on the server. However, the server will accept ``logout`` messages (with no arguments) from logged-in clients:

```
logout;
```

# Server Messages

The server can also send messages to clients, prefaced by the ``server`` username. For example:
```
server error: you must login before sending any other message;
server user: matt joined;
```

# Examples

A simple, “no values” message.
```
all hello_world!;
```

A message with a single value.
```
matt pitch 440;
```

Here, a “note” message, that is defined to always have pitch-value and dynamic-value pairs, in that order.
```
all note pitch 440 dynamic mf;
```

There’s no reason why you couldn't define a message’s parameters to start with a “value” (the “key" implicitly being the message's name).
```
all pitch 440 dynamic mf;
```

Likewise, you could just drop “keys” altogether. Here we pass a csound-like i-statement, where p1=instr, p2=start, p3=dur, p4=cps, etc..
```
matt instr pluck 0 10 440 .5 mp bandpass;
```

This “call” message is defined as a variable-length list of strings.
```
matt call Are we ready to start yet?;
```

In this example, p1 is a blob of JSON (client parsing may be challenging!)
```
all sequence {"notes":[{"start":0,"dur":1,"midi_note":62},{"start":4,"dur":2,"midi_note":68}],"dynamic":"mf"};
```

Byte streams (audio, OSC, et al) could be passed in Base64 format. This is untested - watch out for buffer overflows somewhere in the chain...
```
matt audioBuffer AAECAwUIEBofPCNJcVCSg3QBI5hXEgk4RxApNHgSOVgSMJSDEphHmCQ3Krze/w==;
```

Audio samples might also be passed as individual text parameters. Again, watch out for buffer overflows...
```
matt sample 0 .012345 .198434 .92414132 -.099123 -.0294824 -.33923….;
```

None of the messages demonstrated here are intrinsic to or defined by the server; only the use of usernames (in this case `matt`) and the `all` address are specified by the server itself. Groups of networked users should agree upon the specific message formats and contents that best suit their needs.

# Mirroring

Rather than logging in, a client may instead register to "mirror" network traffic. Mirroring clients receive (almost) all messages sent to and from the server. We anticipate this being useful for users who wish to create visualizations of network traffic; other scenarios may also apply.

To receive all network traffic, a client (connected but not logged-in) sends the message
```
mirror  globalMirroringPassword;
```

Assuming the password matches the password set for the server as an argument at launch, the client will receive all messages sent to the server, in the format
```
sender receiver messageContents;
```
(Note that ordinary logged-in users don't get the `receiver` value included in messages that they receive - this message format is only for `mirror` clients).

Invalid messages are not forwarded to mirrors; neither are `login`, `mirror`, `start`, or `stop` messages. All messages addressed to valid usernames, to `all`, or to keywords registered by an extension (see below) are forwarded. `mirror` commands from logged-in users are disregarded by the server; clients may either `mirror` or `login`, but not both.

# Shadowing

Where mirroring provides access to all network traffic, shadowing provides access to copies of messages addressed to a particular user. We anticipate this being useful for users in different geographical locations who want to run local copies of other users' audio instruments; other scenarios may also apply.

Users can disallow shadowing for their username by declining to specify a password at login. If users do supply a password at login, then other clients can shadow them by sending a login message with the identical username and password.

```
login matt mattsShadowingPassword;
```

Assuming that a user `matt` is already logged in, that `matt` supplied a password when logging in, and that the password supplied by the shadowing client matches, the server will forward a copy of all messages addressed to `matt` to the shadowing client. (The original `matt` user will continue to receive the messages as before). Messages forwarded to the shadow are identical to those sent to the logged-in user. Shadowing clients are forbidden from sending messages of their own. To end shadowing, disconnect from the server. If a logged-in user disconnects, all shadowing of that username also terminates (though network connections will remain open until the shadowing client disconnects).

# Extensions

The server can load node.js modules to extend the basic functionality for customized implementations (pieces). Extensions register callback functions to handle specific messages received by the server. Each module is embodied in a single JavaScript file, stored in the "pieces" subdirectory of the server project.

To load and activate the code in a module, a client user sends a ``start`` keyword with the the module's filename (the .js extension is optional):
```
start examplePiece;
```

To deactivate a module and return the server to its base state, the client user that launched the module needs to send a message with a ``stop`` keyword:
```
stop;
```

Only one module may be active at a time; once a module is loaded, the initiating client must send a ``stop`` message prior to loading another. (Other users are not permitted to issue a ``stop``). Otherwise the server will report an error:
```
server error: must stop currently running piece before starting another;
```

The examplePiece.js file in the "pieces" subdirectory of the repository provides a template for module development (although it does not itself embody any useful functionality). assembler.js and zeroSum.js are full-blown examples embodying the behaviors needed for the structured improvisations *Assembler* and *Zero Sum*. We hope to add additional compositions (and thus examples) to the repository over time.
