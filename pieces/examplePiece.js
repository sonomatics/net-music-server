// include netMusicServer to call its exported functions
// the path here looks for netMusicServer in a parent directory, one level up
var netMusServer = require('./../netMusServer')

// this function is called by netMusicServer to start a piece/extension
// declare and initialize global variables, register handlers,
// add utility functions, etc.
exports.start = function ()
{
    console.log("starting example piece...")

    // get all users currently logged in
    var users = netMusServer.getUsers()

    // send a message to all users
    netMusServer.sendToAll("server", "The example piece is now running!")

    // send a message to a specific user
    if (users[0])
    {
        netMusServer.sendToUser("server", users[0], "Hi " + users[0] + "!", function(err) {
            // an error occured
            console.log(err)
        })
    }

    //==========================================
    // register handler for any message keyword:
    //=========================================

    // handler for all current users
    for (var i = 0; i < users.length; i++ )
    {
        var aUser = users[i]
        console.log("registering handler for current user: " + aUser)
        netMusServer.on(aUser, function(sender, messageContents) {
            console.log(sender + " sent: \'" + messageContents + "\' to " + aUser)
        })
    }

    // handle messages sent to "all"
    netMusServer.on("all", function(sender, messageContents) {
        console.log(sender + " sent: \'" + messageContents + "\' to all users")
    })

    // handle a user logging in
    netMusServer.on("login", function(sender, messageContents) {
        console.log("\'" + sender + "\' just logged in. now registering user handler...")

        // which you could then register a handler for messages sent to this user
        netMusServer.on(sender, function(sender, messageContents) {
            console.log(sender + " sent: \'" + messageContents + "\' to " + sender)
        })
    })

    // handle a user logging out
    netMusServer.on("logout", function(sender, messageContents) {
        console.log("\'" + sender + "\' just logged out.")
    })

    // handle custom keyword - users send "server mode <value>;"
    // note that we use .onServer() to add server keywords
    // note also that you can't register a handler for the "server" address directly
    netMusServer.onServer("mode", function(sender, messageContents) {
        console.log("\'mode\' was set to " + messageContents[0] + " by: " + sender)
    })
}

// this function will be called when the server ends the piece / extension
// do any cleanup here
exports.stop = function ()
{
    console.log("stopping example piece...")
    // send a message to all users
    netMusServer.sendToAll("server", "The example piece is no longer running.")
}

console.log("example piece has been loaded!")
