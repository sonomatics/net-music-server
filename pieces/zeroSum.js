// zeroSum.js
// extends netMusServer.js with messaging behavior / data structures
// for performance of "Zero Sum"
// send a "start zeroSum;" to load this functionality
// and a "stop;" message to return the server to its base state

// include netMusicServer to call its exported functions
// the path here looks for netMusicServer in a parent directory (one level up)
var netMusServer = require('./../netMusServer')

// this function will be called by netMusicServer to start the pieces
// here you will initialize your piece (global data may be in an unknown state
// from a previous run of the piece), and register handlers
exports.start = function ()
{
  var scenario1 = true // true = scenario 1 active, false = scenario 2 active
  var eligiblePitches = []
  // change eligiblePitchCount / basePitch to change range / number of pitches
  var eligiblePitchCount = 88 // decrementing counter of eligible pitches
  var basePitch = 21 // MIDI pitch number corresponding to eligiblePitches[0]
  var mostRecentPitch = -1; // tracks latest eligible pitch delivered
  for (var i = 0; i < eligiblePitchCount; i++) // 88 element boolean array
  // each array element corresponds with a MIDI pitch from 21-108
  {
    // initially, all 88 pitches are eligible for use
    eligiblePitches.push(true)
  }
  var scenario2Pitches = [] // fill with pitches eligible for repetition in scenario2
  // this array is only for use with "getRandom" messages in scenario2
  // for other types of scenario2 pitch requests, use eligiblePitches[]
  // unlike eligiblePitches, this is an array of integer indexes
  // as pitches become temporarily eligible for repetition in scenario 2,
  // their index value (0-87) is added to this array
  var scenario2Index = 0; // used for "repeatIterative"

  netMusServer.sendToAll("server", "Zero Sum is running")
  console.log("Zero Sum initialization complete")
  // performance always begins in scenario 1
  netMusServer.sendToAll("server", "scenario 1")

  // register handler for any message keyword:

  // user message which toggles between the two "Zero Sum" scenarios
  netMusServer.onServer("switchScenario", function(sender, messageContents)
  {
    scenario1 = !scenario1
    if(scenario1)
    {
      scenario2Pitches = [] // flush any pitches previously eligible for repetition
      netMusServer.sendToAll("server", "scenario 1")
    }
    else
    {
      netMusServer.sendToAll("server", "scenario 2")
    }
  })

  // user messages which request a pitch value
  // (eligibility of specific pitches depends upon scenario + events thus far)
  // messages prefixed with "new" request a previously unused pitch
  // messages prefixed with "repeat" request a previously used pitch for repetition, scenario 2 only

  // returns any eligible pitch, chosen randomly: no arguments
  netMusServer.onServer("newRandom", function(sender, messageContents)
  {
    var returnIndex = -1
    returnIndex = searchPitchesBidirectional(Math.floor(Math.random() * eligiblePitches.length), 1)
    reportNew(sender, returnIndex)
  })

  // returns lowest eligible pitch: no arguments
  netMusServer.onServer("newLowest", function(sender, messageContents)
  {
    reportNew(sender, searchPitchesAscending(0, 1))
  })

  // returns highest eligible pitch: no arguments
  netMusServer.onServer("newHighest", function(sender, messageContents)
  {
    reportNew(sender, searchPitchesDescending(eligiblePitches.length - 1, 1))
  })

  // returns eligible pitch nearest to reference pitch (equal, above, or below)
  // one argument: reference pitch
  netMusServer.onServer("newNearest", function(sender, messageContents)
  {
    var searchIndex = parseInt(messageContents[0]) - basePitch
    if(searchIndex >=0 && searchIndex < eligiblePitches.length)
    {
      reportNew(sender, searchPitchesBidirectional(searchIndex, 1))
    }
    else
    {
      netMusServer.sendToUser("server", sender, "invalid arguments to newNearest", function(err) {
        console.log(err)
      })
    }
  })

  // returns eligible pitch at an interval multiple above or below a reference pitch
  // one required argument: interval (> 0, negative values treated as positive)
  // optional second argument: reference pitch (otherwise uses last issued pitch)
  netMusServer.onServer("newInterval", function(sender, messageContents)
  {
    var searchIndex = mostRecentPitch;
    if(messageContents.length >= 2) {
      searchIndex = parseInt(messageContents[1] - basePitch)
    }
    var searchInterval = Math.abs(parseInt(messageContents[0]))
    if(searchIndex >=0 && searchIndex < eligiblePitches.length && searchInterval > 0)
    {
      reportNew(sender, searchIntervalsBidirectional(searchIndex, searchInterval))
    }
    else
    {
      netMusServer.sendToUser("server", sender, "invalid arguments to newInterval", function(err) {
        console.log(err)
      })
    }
  })

  // returns random element from scenario2Pitches array: no arguments
  netMusServer.onServer("repeatRandom", function(sender, messageContents)
  {
    if(scenario1)
    {
      netMusServer.sendToUser("server", sender, "repeated pitches only available in scenario 2", function(err) {
        console.log(err)
      })
    }
    else if(scenario2Pitches.length > 0)
    {
      var randomIndex = Math.floor(Math.random() * scenario2Pitches.length)
      reportRepeated(sender, scenario2Pitches[randomIndex])
      scenario2Index = randomIndex + 1
    }
    else
    {
      netMusServer.sendToUser("server", sender, "no pitches eligible for repetition", function(err) {
        console.log(err)
      })
    }
  })

  // returns "next" element from scenario2Pitches array: no arguments
  netMusServer.onServer("repeatIterative", function(sender, messageContents)
  {
    if(scenario1)
    {
      netMusServer.sendToUser("server", sender, "repeated pitches only available in scenario 2", function(err) {
        console.log(err)
      })
    }
    else if(scenario2Pitches.length > 0)
    {
      if(scenario2Index < scenario2Pitches.length) // if index is in-bounds
      {
        reportRepeated(sender, scenario2Pitches[scenario2Index]) // ...use it
        scenario2Index++  // ...and then increment
      }
      else  // else if index is out-of-bounds
      {
        reportRepeated(sender, scenario2Pitches[0]) // return the first element
        scenario2Index = 1;  // and set the index for the second element
      }
    }
    else
    {
      netMusServer.sendToUser("server", sender, "no pitches eligible for repetition", function(err) {
        console.log(err)
      })
    }
  })

  // helper functions: search for pitches

  // look for pitches at or above a specific index
  // searchIndex is starting index for searchIndex
  // searchIncrement is added to generate next index: useful for interval requests
  function searchPitchesAscending(searchIndex, searchIncrement)
  {
    var returnValue = -1 // search failure condition indicated by -1
    while(searchIndex < eligiblePitches.length && !eligiblePitches[searchIndex])
    {
      searchIndex += searchIncrement
    }
    if(searchIndex < eligiblePitches.length)
    {
      returnValue = searchIndex
    }
    return returnValue
  }

  // look for pitches at or below a specific index
  // searchIndex is starting index for searchIndex
  // searchIncrement is added to generate next index: useful for interval requests
  function searchPitchesDescending(searchIndex, searchDecrement)
  {
    var returnValue = -1 // search failure condition indicated by -1
    while(searchIndex >= 0 && !eligiblePitches[searchIndex])
    {
      searchIndex -= searchDecrement
    }
    if(searchIndex >= 0)
    {
      returnValue = searchIndex
    }
    return returnValue
  }

  // look for pitches at a specific index, and then alternating above and below
  // searchIndex is starting index for searchIndex
  // searchIncrement is added to generate next index: useful for interval requests
  function searchPitchesBidirectional(searchIndex, searchIncrement)
  {
    var returnValue = -1 // search failure condition indicated by -1
    var ascending = searchPitchesAscending(searchIndex, searchIncrement)
    var descending = searchPitchesDescending(searchIndex, searchIncrement)
    if(ascending < 0)
    {
      returnValue = descending
    }
    else if (descending < 0)
    {
      returnValue = ascending
    }
    else if(ascending - searchIndex <= searchIndex - descending)
    {
      returnValue = ascending
    }
    else
    {
      returnValue = descending
    }
    return returnValue
  }

  // look for intervals alternating above and below a specific index
  // searchIndex is starting index for searchIndex
  // searches occur at searchIncrement multiples above and below searchIndex
  function searchIntervalsBidirectional(searchIndex, searchIncrement)
  {
    var returnValue = -1 // search failure condition indicated by -1
    var ascending = searchPitchesAscending(searchIndex + searchIncrement, searchIncrement)
    var descending = searchPitchesDescending(searchIndex - searchIncrement, searchIncrement)
    if(ascending < 0)
    {
      returnValue = descending
    }
    else if (descending < 0)
    {
      returnValue = ascending
    }
    else if(ascending - searchIndex <= searchIndex - descending)
    {
      returnValue = ascending
    }
    else
    {
      returnValue = descending
    }
    return returnValue
  }

  // helper function: after search / provision to user, mark a pitch as used

  // once a pitch is supplied to a user remove it from future eligibility
  // if in scenario 2, make it temporarily available for repetition
  // this is doing most of its work through side effects on global variables...
  function changePitchEligibility(pitchIndex) {
    eligiblePitches[pitchIndex] = false;
    // reduce the count of eligible pitches remaining
    eligiblePitchCount--;
    if(!scenario1) // if in scenario 2...
    {
      // ...then make the pitch eligible for repetition in scenario2
      scenario2Pitches.push(pitchIndex)
    }
  }

  // helper function: report results of searches triggered by "new" pitch requests
  function reportNew(sender, searchResult)
  {
    // return no-result (" pitch 0") on -1, or on search results out of range
    if(searchResult >= 0 && searchResult < eligiblePitches.length && (scenario1 || eligiblePitchCount > 1))
    {
      // make the pitch ineligible for future use
      changePitchEligibility(searchResult)
      // tell all users how many eligible pitches remain
      netMusServer.sendToAll("server", "pitchesRemaining " + eligiblePitchCount)
      // supply the newly found pitch to the requesting user
      mostRecentPitch = searchResult;
      netMusServer.sendToUser("server", sender, "eligiblePitch " + (searchResult + basePitch), function(err) {
        console.log(err)
      })
    }
    // if we looked for a pitch, but didn't find one that met search criteria...
    // or if we are scenario 2 and only one pitch remains...
    else
    {
      // no pitch available, issue no-result message
      netMusServer.sendToUser("server", sender, " eligiblePitch 0", function(err) {
        console.log(err)
      })
    }
  }

  // helper function: report results of searches triggered by "repeat" pitch requests
  function reportRepeated(sender, searchResult)
  {
    // return no-result (" pitch 0") on -1, or on search results out of range
    if(searchResult >= 0 && searchResult < eligiblePitches.length)
    {
      // supply the newly found pitch to the requesting user
      mostRecentPitch = searchResult;
      netMusServer.sendToUser("server", sender, "repeatedPitch " + (searchResult + basePitch), function(err) {
        console.log(err)
      })
    }
    // if we looked for a pitch, but didn't find one that met search criteria...
    else
    {
      // no pitch available, issue no-result message
      netMusServer.sendToUser("server", sender, " repeatedPitch 0", function(err) {
        console.log(err)
      })
    }
  }
}

// this function called when the server ends the piece
// no specific cleanup required for "Zero Sum"
exports.stop = function ()
{
  console.log("stopping Zero Sum...")
  netMusServer.sendToAll("server", "Zero Sum is terminated")
}

console.log("Zero Sum load complete")
