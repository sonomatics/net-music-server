// assembler.js
// extends netMusServer.js with messaging behavior / data structures
// for performance of "Assembler"
// send a "start assembler;" message to load this functionality
// and a "stop;" message to return the server to its base state

// include netMusicServer to call its exported functions
// the path here looks for netMusicServer in a parent directory (one level up)
var netMusServer = require('./../netMusServer')

// this function will be called by netMusicServer to start the pieces
// here you will initialize your piece (global data may be in an unknown state
// from a previous run of the piece), and register handlers
exports.start = function ()
{
    var eventCount = 20 // each client implements a palette of 20 sounds
    var users = netMusServer.getUsers() // array of usernames
    var timer // will hold the active timer object when playback starts
    var isPlaying = false

    // declare and initialize arrays for pattern handling
    var eventArray = [] // event indexes, between 1 and eventCount inclusive
    var interonsetArray = [] // interonset times (in sec) associated with events
    var userArray = [] // usernames associated with events
    var eventIndex = 0 // index for playback location in each array
    var interonsetIndex = 0
    var userIndex = 0
    // generate initial contents of arrays randomly
    for (var i = 0; i < Math.floor((Math.random() * 7) + 1); i++)
    {
        eventArray.push(Math.floor((Math.random() * eventCount) + 1))
        interonsetArray.push(Math.min(Math.random() * 1.88, Math.random() * 1.88, Math.random() * 1.88) + 0.12)
        userArray.push(users[Math.floor(Math.random() * users.length)])
    }
    // notify users about initial contents of arrays
    netMusServer.sendToAll("server", "status events " + eventArray.join(" "))
    netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
    netMusServer.sendToAll("server", "status users " + userArray.join(" "))

    // register handlers for server keywords:

    // client message: add values to pattern handling arrays
    netMusServer.onServer("add", function(sender, messageContents)
    {
        if(messageContents.length == 4)
        {
            if(messageContents[0] == "triple" && validateTriple(messageContents))
            {
                // "server add triple 12 0.5 chris"
                eventArray.push(messageContents[1])
                interonsetArray.push(messageContents[2])
                userArray.push(messageContents[3])
                netMusServer.sendToAll("server", "status events " + eventArray.join(" "))
                netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
                netMusServer.sendToAll("server", "status users " + userArray.join(" "))
            }
            else
            {
                netMusServer.sendToUser("server", sender, "invalid arguments to add", function(err) {
                    console.log(err)
                })
            }
        }
        else if(messageContents.length == 2)
        {
            if(messageContents[0] == "event" && validateEvent(messageContents[1]))
            {
                // "server add event 7"
                eventArray.push(messageContents[1])
                netMusServer.sendToAll("server", "status events " + eventArray.join(" "))
            }
            else if(messageContents[0] == "interonset" && validateInteronset(messageContents[1]))
            {
                // "server add interonset 1.2"
                interonsetArray.push(messageContents[1])
                netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
            }
            else if(messageContents[0] == "user" && validateUser(messageContents[1]))
            {
                // "server add user matt"
                userArray.push(messageContents[1])
                netMusServer.sendToAll("server", "status users " + userArray.join(" "))
            }
            else
            {
                netMusServer.sendToUser("server", sender, "invalid arguments to add", function(err) {
                    console.log(err)
                })
            }
        }
        else if(messageContents.length == 1)
        {
            if(messageContents[0] == "triple") // "server add triple"
            {
                eventArray.push(Math.floor((Math.random() * eventCount) + 1))
                interonsetArray.push(Math.min(Math.random() * 1.88, Math.random() * 1.88, Math.random() * 1.88) + 0.12)
                userArray.push(users[Math.floor(Math.random() * users.length)])
                netMusServer.sendToAll("server", "status events " + eventArray.join(" "))
                netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
                netMusServer.sendToAll("server", "status users " + userArray.join(" "))
            }
            else if(messageContents[0] == "event") // "server add event"
            {
                eventArray.push(Math.floor((Math.random() * eventCount) + 1))
                netMusServer.sendToAll("server", "status events " + eventArray.join(" "))
            }
            else if(messageContents[0] == "interonset") // "server add interonset"
            {
                interonsetArray.push(Math.min(Math.random() * 1.88, Math.random() * 1.88, Math.random() * 1.88) + 0.12)
                netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
            }
            else if(messageContents[0] == "user") // "server add user"
            {
                userArray.push(users[Math.floor(Math.random() * users.length)])
                netMusServer.sendToAll("server", "status users " + userArray.join(" "))
            }
            else
            {
                netMusServer.sendToUser("server", sender, "invalid arguments to add", function(err) {
                    console.log(err)
                })
            }
        }
        else
        {
            netMusServer.sendToUser("server", sender, "invalid arguments to add", function(err) {
                console.log(err)
            })
        }
    })

    // client message: remove values from pattern handling arrays
    netMusServer.onServer("remove", function(sender, messageContents)
    {
        if(messageContents.length == 1)
        {
            if(messageContents[0] == "triple")  // "server remove triple"
            {
                if(eventArray.length > 1)
                {
                    eventArray.shift()
                    netMusServer.sendToAll("server", "status events " + eventArray.join(" "))
                }
                if(interonsetArray.length > 1)
                {
                    interonsetArray.shift()
                    netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
                }
                if(userArray.length > 1)
                {
                    userArray.shift()
                    netMusServer.sendToAll("server", "status users " + userArray.join(" "))
                }
            }
            else if(messageContents[0] == "event") // "server remove event"
            {
                if(eventArray.length > 1)
                {
                    eventArray.shift()
                    netMusServer.sendToAll("server", "status events " + eventArray.join(" "))
                }
            }
            else if(messageContents[0] == "interonset") // "server remove interonset"
            {
                if(interonsetArray.length > 1)
                {
                    interonsetArray.shift()
                    netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
                }
            }
            else if(messageContents[0] == "user") // "server remove user"
            {
                if(userArray.length > 1)
                {
                    userArray.shift()
                    netMusServer.sendToAll("server", "status users " + userArray.join(" "))
                }
            }
            else
            {
                netMusServer.sendToUser("server", sender, "invalid arguments to remove", function(err) {
                  console.log(err)
                })
            }
        }
        else
        {
            netMusServer.sendToUser("server", sender, "invalid arguments to remove", function(err) {
                console.log(err)
            })
        }
    })

    // client message: alter values in pattern handling arrays
    netMusServer.onServer("mutate", function(sender, messageContents)
    {
        if(messageContents.length == 4)
        {
            if(messageContents[0] == "triple" && validateTriple(messageContents))
            {
                // "server mutate triple 7 0.2 chris"
                let minLength = Math.min(eventArray.length, interonsetArray.length, userArray.length)
                let mutateIndex = Math.floor(Math.random() * minLength)
                eventArray[mutateIndex] = messageContents[1]
                interonsetArray[mutateIndex] = messageContents[2]
                userArray[mutateIndex] = messageContents[3]
                netMusServer.sendToAll("server", "status events " + eventArray.join(" "))
                netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
                netMusServer.sendToAll("server", "status users " + userArray.join(" "))
            }
            else
            {
                netMusServer.sendToUser("server", sender, "invalid arguments to mutate", function(err) {
                    console.log(err)
                })
            }
        }
        else if(messageContents.length == 2)
        {
            if(messageContents[0] == "event" && validateEvent(messageContents[1]))
            {
                // "server mutate event 3"
                let mutateIndex = Math.floor(Math.random() * eventArray.length)
                eventArray[mutateIndex] = messageContents[1]
                netMusServer.sendToAll("server", "status events " + eventArray.join(" "))
            }
            else if(messageContents[0] == "interonset" && validateInteronset(messageContents[1]))
            {
                // "server mutate interonset 0.7"
                let mutateIndex = Math.floor(Math.random() * interonsetArray.length)
                interonsetArray[mutateIndex] = messageContents[1]
                netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
            }
            else if(messageContents[0] == "user" && validateUser(messageContents[1]))
            {
                // "server mutate user chris"
                let mutateIndex = Math.floor(Math.random() * userArray.length)
                userArray[mutateIndex] = messageContents[1]
                netMusServer.sendToAll("server", "status users " + userArray.join(" "))
            }
            else
            {
                netMusServer.sendToUser("server", sender, "invalid arguments to mutate", function(err) {
                    console.log(err)
                })
            }
        }
        else if(messageContents.length == 1)
        {
            if(messageContents[0] = "triple")
            {
                // "server mutate triple"
                let minLength = Math.min(eventArray.length, interonsetArray.length, userArray.length)
                let mutateIndex = Math.floor(Math.random() * minLength)
                eventArray[mutateIndex] = Math.floor((Math.random() * eventCount) + 1)
                interonsetArray[mutateIndex] = Math.min(Math.random() * 1.88, Math.random() * 1.88, Math.random() * 1.88) + 0.12
                userArray[mutateIndex] = users[Math.floor(Math.random() * users.length)]
                netMusServer.sendToAll("server", "status events " + eventArray.join(" "))
                netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
                netMusServer.sendToAll("server", "status users " + userArray.join(" "))
            }
            else if(messageContents[0] == "event")
            {
                // "server mutate event"
                let mutateIndex = Math.floor(Math.random() * eventArray.length)
                eventArray[mutateIndex]= Math.floor((Math.random() * eventCount) + 1)
                netMusServer.sendToAll("server", "status events " + eventArray.join(" "))
            }
            else if(messageContents[0] == "interonset")
            {
                // "server mutate interonset"
                let mutateIndex = Math.floor(Math.random() * interonsetArray.length)
                interonsetArray[mutateIndex] = Math.min(Math.random() * 1.88, Math.random() * 1.88, Math.random() * 1.88) + 0.12
                netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
            }
            else if(messageContents[0] == "user")
            {
                // "server mutate user"
                let mutateIndex = Math.floor(Math.random() * userArray.length)
                userArray[mutateIndex] = users[Math.floor(Math.random() * users.length)]
                netMusServer.sendToAll("server", "status users " + userArray.join(" "))
            }
            else
            {
                netMusServer.sendToUser("server", sender, "invalid arguments to mutate", function(err) {
                    console.log(err)
                })
            }
        }
        else
        {
            netMusServer.sendToUser("server", sender, "invalid arguments to mutate", function(err) {
                console.log(err)
            })
        }
    })

    // client message: copy values in pattern handling arrays
    netMusServer.onServer("duplicate", function(sender, messageContents)
    {
        if(messageContents.length == 1)
        {
            if(messageContents[0] == "triple")  // "server duplicate triple"
            {
                let minLength = Math.min(userArray.length, interonsetArray.length, userArray.length)
                let duplicateIndex = Math.floor(Math.random() * minLength)
                eventArray.push(eventArray[duplicateIndex])
                interonsetArray.push(interonsetArray[duplicateIndex])
                userArray.push(userArray[duplicateIndex])
                netMusServer.sendToAll("server", "status events " + eventArray.join(" "))
                netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
                netMusServer.sendToAll("server", "status users " + userArray.join(" "))
            }
            else if(messageContents[0] == "event") // "server duplicate event"
            {
                let duplicateIndex =  Math.floor(Math.random() * eventArray.length)
                eventArray.push(eventArray[duplicateIndex])
                netMusServer.sendToAll("server", "status events " + eventArray.join(" "))
            }
            else if(messageContents[0] == "interonset") // "server duplicate interonset"
            {
                let duplicateIndex =  Math.floor(Math.random() * interonsetArray.length)
                interonsetArray.push(interonsetArray[duplicateIndex])
                netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
            }
            else if(messageContents[0] == "user") // "server duplicate user"
            {
                let duplicateIndex =  Math.floor(Math.random() * userArray.length)
                userArray.push(userArray[duplicateIndex])
                netMusServer.sendToAll("server", "status users " + userArray.join(" "))
            }
            else
            {
                netMusServer.sendToUser("server", sender, "invalid arguments to duplicate", function(err) {
                  console.log(err)
                })
            }
        }
        else
        {
            netMusServer.sendToUser("server", sender, "invalid arguments to duplicate", function(err) {
              console.log(err)
            })
        }
    })

    // client message: scale all values in interonset array
    // "server scale 1.2"
    netMusServer.onServer("scale", function(sender, messageContents)
    {
        if(messageContents.length != 1 || isNaN(messageContents[0]) || messageContents[0] <= 0)
        {
            netMusServer.sendToUser("server", sender, "invalid arguments to scale", function(err) {
              console.log(err)
            })
        }
        else
        {
            for(i = 0; i < interonsetArray.length; i++)
            {
                interonsetArray[i] = Math.max(interonsetArray[i] * messageContents[0], 0.02)
            }
            netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
        }
    })

    // client message: randomized scaling of all values in interonset array
    // "server skew 0.8 1.25"
    netMusServer.onServer("skew", function(sender, messageContents)
    {
        if(messageContents.length != 2 || isNaN(messageContents[0])
        || isNaN(messageContents[1]) || messageContents[0] <=0 || messageContents[1] <=0)
        {
            netMusServer.sendToUser("server", sender, "invalid arguments to skew", function(err) {
              console.log(err)
            })
        }
        else
        {
            let scaleCeil = Math.max(messageContents[0], messageContents[1])
            let scaleFloor = Math.min(messageContents[0], messageContents[1])
            let scaleAmount = scaleCeil - scaleFloor
            for(i = 0; i < interonsetArray.length; i++)
            {
                let scaleValue = (Math.random() * scaleAmount) + scaleFloor
                interonsetArray[i] = Math.max(interonsetArray[i] * scaleValue, 0.02)
            }
            netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
        }
    })

    // client message: replace contents of event, interonset, or user array
    netMusServer.onServer("replace", function(sender, messageContents)
    {
        if(messageContents[0] == "event")
        {
            if(messageContents.length > 1) // "server replace event 12 7 8 9"
            {
                // validate each event value supplied by client
                messageContents.shift() // remove "event" keyword
                let eventsValid = true
                let i = 0
                while(i < messageContents.length && eventsValid)
                {
                    eventsValid = validateEvent(messageContents[i])
                    i++
                }
                if(eventsValid)
                {
                    eventArray = messageContents // replace previous eventArray
                    netMusServer.sendToAll("server", "status events " + eventArray.join(" "))
                }
                else {
                    netMusServer.sendToUser("server", sender, "invalid arguments to replace", function(err) {
                      console.log(err)
                    })
                }
            }
            else // "server replace event"
            {
                eventArray = [] // wipe out previous array, then regenerate
                for (var i = 0; i < Math.floor((Math.random() * 7) + 1); i++)
                {
                    eventArray.push(Math.floor((Math.random() * eventCount) + 1))
                }
                netMusServer.sendToAll("server", "status events " + eventArray.join(" "))
            }
        }
        else if(messageContents[0] == "interonset")
        {
            {
                if(messageContents.length > 1) // "server replace interonset 0.2 0.4 0.6 0.2"
                {
                    // validate each interonset value supplied by client
                    messageContents.shift() // remove "interonset" keyword
                    let interonsetsValid = true
                    let i = 0
                    while(i < messageContents.length && interonsetsValid)
                    {
                        interonsetsValid = validateInteronset(messageContents[i])
                        i++
                    }
                    if(interonsetsValid)
                    {
                        interonsetArray = messageContents // replace previous interonset array
                        netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
                    }
                    else {
                        netMusServer.sendToUser("server", sender, "invalid arguments to replace", function(err) {
                          console.log(err)
                        })
                    }
                }
                else // "server replace interonset"
                {
                    interonsetArray = [] // wipe out previous array, then regenerate
                    for (var i = 0; i < Math.floor((Math.random() * 7) + 1); i++)
                    {
                        interonsetArray.push(Math.min(Math.random() * 1.88, Math.random() * 1.88) + 0.12)
                    }
                    netMusServer.sendToAll("server", "status interonsets " + interonsetArray.join(" "))
                }
            }
        }
        else if(messageContents[0] == "user")
        {
            if(messageContents.length > 1) // "server replace user matt chris matt bill"
            {
                // validate each username supplied by client
                messageContents.shift() // remove "user" keyword
                let usersValid = true
                let i = 0
                while(i < messageContents.length && usersValid)
                {
                    usersValid = validateUser(messageContents[i])
                    i++
                }
                if(usersValid)
                {
                    userArray = messageContents  // replace previous username array
                    netMusServer.sendToAll("server", "status users " + userArray.join(" "))
                }
                else {
                    netMusServer.sendToUser("server", sender, "invalid arguments to replace", function(err) {
                      console.log(err)
                    })
                }
            }
            else // "server replace user"
            {
                userArray = [] // wipe out previous array, then regenerate
                for (var i = 0; i < Math.floor((Math.random() * 7) + 1); i++)
                {
                    userArray.push(users[Math.floor(Math.random() * users.length)])
                }
                netMusServer.sendToAll("server", "status users " + userArray.join(" "))
            }
        }
        else
        {
            netMusServer.sendToUser("server", sender, "invalid arguments to replace", function(err) {
              console.log(err)
            })
        }
    })

    // client message: start/resume pattern playback
    netMusServer.onServer("play", function(sender, messageContents)
    {
        if(timer && isPlaying)
        {
            netMusServer.sendToUser("server", sender, "playback active, play message ignored", function(err) {
              console.log(err)
            })
        }
        else
        {
            netMusServer.sendToAll("server", "status playing")
            outputEvent() // helper function which manages timing/callbacks
            isPlaying = true // update playback status
        }
    })

    // client message: pause/stop pattern playback
    netMusServer.onServer("pause", function(sender, messageContents)
    {
        netMusServer.sendToAll("server", "status paused")
        if(timer && isPlaying) // if there is an active timer...
        {
            clearTimeout(timer) // ... then cancel pending callback
            isPlaying = false // and update playback status
        }
    })

    // handle a user logging in
    netMusServer.on("login", function(sender, messageContents) {
        users.push(sender) // add user to array of valid usernames
    })

    // handle a user logging out
    netMusServer.on("logout", function(sender, messageContents) {
        let userIndex = users.indexOf(sender)
        // remove departing user from array of valid usernames
        if(userIndex >= 0)
        {
            users.splice(userIndex, 1)
        }
        // if no users remain, stop playback
        if(users.length < 1)
        {
            if(timer) {
                clearTimeout(timer)
                console.log("assembler pausing playback: no users present")
            }
        }
        // otherwise, replace departing user with randomly selected valid users
        else
        {
            for(i = 0; i < userArray.length; i++)
            {
                if(userArray[i] == sender)
                {
                    userArray[i] = users[Math.floor(Math.random() * users.length)]
                }
            }
        }
    })

    // helper function: validate "triple" values
    function validateTriple(prefixedTriple) {
        // prefixedTriple contains four values: prefix, event, interonset, user
        return validateEvent(prefixedTriple[1]) && validateInteronset(prefixedTriple[2]) && validateUser(prefixedTriple[3])
    }

    // helper function: validate event values
    function validateEvent(eventValue) {
        return eventValue > 0 && eventValue <= eventCount && eventValue % 1 == 0
    }

    // helper function: validate interonset values
    function validateInteronset(interonsetValue) {
        return interonsetValue >= 0.02
    }

    // helper function: validate user values
    function validateUser(userValue) {
        return users.includes(userValue)
    }

    // helper function: send event messages and manage timing/playback
    function outputEvent()
    {
        // check validity of array indices
        if(eventIndex >= eventArray.length)
        {
            eventIndex = 0
        }
        if(interonsetIndex >= interonsetArray.length)
        {
            interonsetIndex = 0
        }
        if(userIndex >= userArray.length)
        {
            userIndex = 0
        }

        // send event message to user
        let next = interonsetArray[interonsetIndex]
        let eventString = "event " + eventArray[eventIndex] + " maxdur " + next
        if(userArray[userIndex] == "all")
        {
            netMusServer.sendToAll("server", eventString)
        }
        else
        {
            netMusServer.sendToUser("server", userArray[userIndex], eventString, function(err) {
              console.log(err)
            })
        }

        // advance indices
        eventIndex++
        interonsetIndex++
        userIndex++

        // set callback for next event
        timer = setTimeout(outputEvent, next * 1000)
    }

    // notify users that initialization is complete
    netMusServer.sendToAll("server", "Assembler is running")
    console.log("Assembler initialization complete")
}

// this function called when the server ends the piece
// no specific cleanup required for "Zero Sum"
exports.stop = function ()
{
    console.log("stopping Assembler...")
    netMusServer.sendToAll("server", "Assembler is terminated")
}

console.log("Assembler load complete")
