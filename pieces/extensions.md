# Extensions to net-music-server

The net-music-server can be extended using Node.js modules. We envision these extensions as being useful primarily for the implementation of specific compositions or structured improvisations which require logic or data structures beyond the basic capabilities provided by the base server.

Extensions register functions which handle specific messages received by the server. Each module is embodied in a single JavaScript file, stored in the "pieces" subdirectory of the server project.

# Registering handlers

The base server supplies .on() and .onServer() functions which can be used to trigger behavior when messages invoking specific, pre-registered usernames or keywords are sent to the server. Typically these functions will be defined inside the ``exports.start`` block of the module.

For instance, this code registers a handler for any messages which are sent to the "all" keyword/address. `sender` is the original sender of the message, and `messageContents` is an array holding the remaining parameters of the message (with spaces used to define the extents of each array element. The array items are thus "word-by-word"):

```
exports.start = function ()
{
    // handle messages sent to "all"
    netMusServer.on("all", function(sender, messageContents) {
        console.log(sender + " sent: \'" + messageContents + "\' to all users")
    })
}
```

The .on() function may be used to register handlers for specific usernames (as destinations / addresses), and for messages which begin with the  ``login``, ``logout``, ``all``, or ``mirror`` keywords.

The .onServer() function may be used to register additional keywords as desired, as long as those keywords are sent to the ``server`` address. *Zero Sum* makes extensive use of this feature. For example, a message such as:

```
server switchScenario;
```

is registered in the zeroSum.js extension module using this code:

```
exports.start = function ()
{
    netMusServer.onServer("switchScenario", function(sender, messageContents)
    {
        scenario1 = !scenario1
        if(scenario1)
        {
            netMusServer.sendToAll("server", "scenario 1")
        }
        else
        {
            netMusServer.sendToAll("server", "scenario 2")
        }
    })
}
```

# Client and login data

The base server exports a function named .getUsers(). To populate an array with a complete list of current client usernames, use:

```
var users = netMusServer.getUsers()
```

You could then register handlers for each username in the array, using the .on() function:

```
for (var i = 0; i < users.length; i++ )
{
    var aUser = users[i]
    console.log("registering handler for current user: " + aUser)
    netMusServer.on(aUser, function(sender, messageContents) {
        console.log(sender + " sent: \'" + messageContents + "\' to " + aUser)
    })
}
```

To capture client login and logout events as they occur, register handlers for the "login" and "logout" keywords:

```
exports.start = function ()
{
    // handle a user logging in
    netMusServer.on("login", function(sender, messageContents) {
        console.log("\'" + sender + "\' just logged in. now registering user handler...")

        // you could then register a handler for messages sent to this newly logged-in user
        netMusServer.on(sender, function(sender, messageContents) {
            console.log(sender + " sent: \'" + messageContents + "\' to " + sender)
        })
    })

    // handle a user logging out
    netMusServer.on("logout", function(sender, messageContents) {
        console.log("\'" + sender + "\' just logged out.")
    })
}
```

Note that the "logout" handler will be called both when clients disconnect their socket (an "implicit" logout) and when they issue the ``logout`` keyword in a message (an "explicit" logout).

# Loading and initializing an extension

To load and activate the code in an extension module, a client user sends a ``start`` keyword with the the module's filename (the .js extension to the filename is optional):
```
start examplePiece;
```

When the ``start`` keyword is issued, any code in the module file inside the ``exports.start`` block will run:

```
exports.start = function ()
{
    // initialization code here:
    // register handlers for keywords and usernames
    // declare and initialize any data structures shared across callbacks
    // check for logged-in clients at module startup
    // etc.
}
```

# Terminating an extension

To deactivate an extension module and return the server to its base state, the client user that launched the module needs to send a message with a ``stop`` keyword:
```
stop;
```

Only one module may be active at a time; once a module is loaded, the initiating client must send a ``stop`` message prior to loading another. (Other users are not permitted to issue a ``stop``). Otherwise the server will report an error:
```
server error: must stop currently running piece before starting another;
```

When the ``stop`` keyword is issued, any code in the module file inside the ``exports.stop`` block will run:

```
exports.stop = function ()
{
    // termination code here
    // may be useful for closing files, warning clients, etc.
}
```

# Templates and examples

The examplePiece.js file in the "pieces" subdirectory of the repository provides a template for module development (although it does not itself embody any useful functionality). assembler.js and zeroSum.js are full-blown examples embodying the behaviors needed for the structured improvisations *Assembler* and *Zero Sum*. We hope to add additional compositions (and thus examples) to the repository over time.
