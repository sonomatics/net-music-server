//
//  AppleNetworking.hpp
//  NetMusicMac
//
//  Created by matt ingalls on 9/3/18.
//  Copyright © 2018 sonomatics. All rights reserved.
//

#ifndef NetMusic_AppleNetworking_h
#define NetMusic_AppleNetworking_h

#include <CFNetwork/CFNetwork.h>
#include <netinet/in.h>  // IPPROTO_TCP
#include <netinet/tcp.h>  // TCP_NODELAY

#include <chrono>
#include <thread>

// read and send FUDI data
class NetMusicStream
{
public:
    
    NetMusicStream(std::string hostAddress, int port, bool noDelay, std::function<void(std::string &sender, std::string &msg)> readCallback)
    {
        CFStringRef addr = CFStringCreateWithCString(kCFAllocatorDefault, hostAddress.c_str(),kCFStringEncodingUTF8);
        CFHostRef host = CFHostCreateWithName(kCFAllocatorDefault, addr);
        
        CFStreamCreatePairWithSocketToCFHost(kCFAllocatorDefault, host, port, &mReadStream, &mWriteStream);
        
        CFWriteStreamOpen(mWriteStream);
        CFReadStreamOpen(mReadStream);
        
        // wait for the streams to open
        while (mWriteStatus != kCFStreamStatusOpen)
        {
            mWriteStatus = CFWriteStreamGetStatus(mWriteStream);
            if (mWriteStatus == kCFStreamStatusError)
                return;
            
            Sleep(.01);
        }
        
        while (mReadStatus != kCFStreamStatusOpen)
        {
            mReadStatus = CFReadStreamGetStatus(mReadStream);
            if (mReadStatus == kCFStreamStatusError)
                return;
            
            Sleep(.01);
        }

        // disable nagle
        if (noDelay)
        {
            CFDataRef data = (CFDataRef)CFReadStreamCopyProperty(mReadStream, kCFStreamPropertySocketNativeHandle);
            
            int noDelay = 1;
            int sock;
            CFDataGetBytes(data, CFRangeMake(0, CFDataGetLength(data)), (UInt8 *)&sock);
            int ret = setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, &noDelay, sizeof(noDelay));
            
            data = (CFDataRef)CFWriteStreamCopyProperty(mWriteStream, kCFStreamPropertySocketNativeHandle);
            CFDataGetBytes(data, CFRangeMake(0, CFDataGetLength(data)), (UInt8 *)&sock);
            ret = setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, &noDelay, sizeof(noDelay));
        }
        
        // streams are open, start our read thread
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            UInt8 buffer[4096];
            std::string sender;
            std::string message;
            bool senderComplete = false;
            
            while (true)
            {
                while (CFReadStreamHasBytesAvailable(mReadStream))
                {
                    CFIndex len = CFReadStreamRead(mReadStream, buffer, 4095);
                    
                    // parse input.  we make the assumption that all whitespace is an actual space (' ')
                    // and that there are not any sections with multiple sapces.
                    for (int i = 0; i < len; i++)
                    {
                        if (buffer[i] == ';')
                        {
                            readCallback(sender, message);
                            message.clear();
                            sender.clear();
                            senderComplete = false;
                        }
                        else if (senderComplete)
                        {
                            message += buffer[i];
                        }
                        else if (buffer[i] == ' ')
                        {
                            senderComplete = true;
                        }
                        else
                        {
                            sender += buffer[i];
                        }
                    }
                }
                
                // let's poll every 10 milliseconds
                Sleep(.01);
            }
        });
        
        CFRelease(addr);
    }
    
    virtual ~NetMusicStream()
    {
        CFReadStreamClose(mReadStream);
        CFWriteStreamClose(mWriteStream);
    }
    
    // send a message to a user/keyword. this function automatically adds the terminating semicolon.
    void Send(std::string destination, std::string message = "")
    {
        destination += " " + message + ";";
        Send((const UInt8 *)destination.c_str(), (int)destination.length());
    }
    
    // C-style message.  caller must make sure the message ends with a terminating semicolon
    void Send(const unsigned char *text, int length)
    {
        CFWriteStreamWrite(mWriteStream, text, length);
    }
    
    
private:
    
    CFReadStreamRef mReadStream;
    CFWriteStreamRef mWriteStream;
    CFStreamStatus mWriteStatus = kCFStreamStatusNotOpen;
    CFStreamStatus mReadStatus = kCFStreamStatusNotOpen;
    
    void Sleep(float secs)
    {
        std::this_thread::sleep_for(std::chrono::microseconds((long long)(1000000 * secs)));
    }
};

#endif /* NetMusic_AppleNetworking_h */
