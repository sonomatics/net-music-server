//
//  MacUtilities.h
//  NetMusicMac
//
//  Created by matt ingalls on 9/28/13.
//  Copyright (c) 2013 sonomatics. All rights reserved.
//

#ifndef NetMusic_MacUtilities_h
#define NetMusic_MacUtilities_h

#include <atomic>

// Key-by-Key input from command line
class KeyboardInput
{
public:
    KeyboardInput()
    {
        // start keystroke capture
        // read from commandline in another thread
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            system("/bin/stty raw"); // do not buffer
            mOn = true;
            while (mOn)
            {
                mChars[mWriteIndex++] = getchar();
                if (mWriteIndex == kBufSize)
                    mWriteIndex = 0;
            }
        });
    }
    
    ~KeyboardInput()
    {
        mOn = false;
        system ("/bin/stty cooked"); // return to normal
    }
    
    char LastChar()
    {
        // if read is same as write, we have no new input
        if (mReadIndex == mWriteIndex)
            return 0;
        
        char c = mChars[mReadIndex++];
        if (mReadIndex == kBufSize)
            mReadIndex = 0;
        
        return c;
    }
    
    std::atomic_bool mOn{false};
    
    // ring buffer
    const static int kBufSize = 128;
    char mChars[kBufSize];
    std::atomic_uint mReadIndex{0};
    std::atomic_uint mWriteIndex{0};
};

#endif // NetMusic_MacUtilities_h
