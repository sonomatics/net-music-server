//
//  main.cpp
//  NetMusicMac
//
//  Created by matt ingalls on 4/19/20.
//  Copyright © 2020 sonomatics. All rights reserved.
//

#include <iostream>
#include <sstream>
#include <vector>

#include "AppleNetworking.hpp"
#include "MacUtilities.hpp"

int main(int argc, const char * argv[])
{
    if (argc != 2)
    {
        std::cout << "no username given!" << std::endl;
        return 0;
    }
    
    NetMusicStream *theStream = new NetMusicStream("localhost", 8124, true, [&](std::string &sender, std::string &msg) {

        std::cout << sender << " sent: " << msg << std::endl;
        
        // parse message params into a vector
        std::istringstream iss(msg);
        std::vector<std::string> msgParams(std::istream_iterator<std::string>{iss}, std::istream_iterator<std::string>());

        
        // ... respond to the message here! ...
        
    });
    
    theStream->Send("login", argv[1]);

    std::cout << "Now you can enter a message! (return will send, ctrl-D quits)" << std::endl;

    KeyboardInput inKey;
    char inChar;
    std::string outMsg;
    
    bool allOK = true;
    while (allOK)
    {
        inChar = inKey.LastChar();
        
        if (inChar == 0x0D) // return key sends
        {
            if (outMsg.length() > 0)
            {
                theStream->Send(outMsg);
                outMsg.clear();
            }
        }
        else if (inChar == 0x08 || inChar == 0x7F) // delete
        {
            if (outMsg.length() > 0)
            {
                outMsg.pop_back();
            }
        }
        else if (inChar == 0x04) // cntrl-D
        {
            allOK = false;
        }
        else if (inChar != 0) // save the char
        {
            outMsg += inChar;
        }
        
        usleep(10000);
    }
    
    return 0;
}
